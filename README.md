# Reposs
Reposs is a repo based torrent engine deisgend for whatever the user want's just pls respect copyright law.

# Help wanted
If anyone knows how to get flatpaks to compile with this pls comminet the changes so I can add it compiling to everything kills this shitty mac.

## Installing - Build 
To install open a new terminal and run the following commands.

1) Git clone the main branch.

```bash
git clone -b main https://gitlab.com/HttpAnimations/reposs.git
```

2) Enter the new dir.

```bash
cd Reposs
```

3) Install the needed packages.


```bash
npm install
```

4) Build for your system.

```bash
npm run make
```

If you are only gonna deploy on this system then your good hear you are free to install the package in the out dir.

5) Build for all systems Linux/macOS/NT.

```bash
npm run build
```

# Installing - Binary
This will go over installing from a bianry.

1) Go to [here](https://gitlab.com/HttpAnimations/reposs/-/releases).

2) Download the bianry for your system am on **[Manjaro](manjaro.org/)** so am gonna use the basic bianary.

3) Extract if needed.

4) Install - Deb

```bash
dpkg -i file.deb
```

5) Install - Rpm

```bash
sudo dnf install file.rpm
```

6) Install - Basic Linux

Extract the zip then run **Reposs** from the extracted folder. 

7) macOS
Extract and put into /Applications



8) Install - NT

Run the .exe file


# Pass over
This part of the guide will show you how to pass over some issues with some distros.


# Repos
Repos are the way of providing software and media.

## Adding repos
To add a repo open Reposs and go to Settings and then the type of repo you want to add now you can add any repo to anything as they all use the same base but note if you do that your torrents won't be orginzed for this we will add a movie repo so go to **Reposs/Settings/Movies/** you will see something like this screen.

![photo](images/Screenshot%202024-05-03%20at%2012.24.09.png)

Where it says *Enter a new repo URL* enter the url for your new repo for this we will use the [public domain repo for movies](https://gitlab.com/HttpAnimations/reposs/-/tree/Public-Repos?ref_type=heads) this will be the deafult repo you have if you want to get more repos you can go [here](https://gitlab.com/HttpAnimations/reposs/-/tree/Repos?ref_type=heads) witch has every known repo but note these might contain media that is copyrighted none of them are tested for the content only if it works the only ones that are tested are the offical ones but with aside paste in the url.

![photo](images/Screenshot%202024-05-03%20at%2012.29.32.png)

Now click on *Add Repo* to add your new repo you will need write premissions to edit the file.

![photo](images/Screenshot%202024-05-03%20at%2012.32.47.png)

## Removing repos
If you would like to remove a repo either becuase it's broken laggy or malware or just don't want it anymore you can do so by opening the settings page and then click on the thing you wanna move eg. Moveis, ~~porn~~ TV. To do this go to **Reposs/Settings/MEDIA** eg. moves **Reposs/Settings/Movies** now you will see a screen like this one.

![photo](images/Screenshot%202024-05-03%20at%2012.37.18.png)

Once here click on the remove button

![photo](images/Screenshot%202024-05-03%20at%2012.38.07.png)

![photo](images/Screenshot%202024-05-03%20at%2012.38.23.png)


## Making a new repo
If you would like to make a new repo you need the following

1) Basic **json** knowgle

2) IDE | [VSC](https://code.visualstudio.com/)

3) touch

4) :3

<br>

- Make a new folder.
```bash
mkdir ~/RepoEXP
```

- Open the folder.
```bash
cd ~/RepoEXP
```

- Open the folder in a IDE 
```bash
code .
```

- Make a new file called what ever you want for this we want movies repo to do make a new file called **Movies.json**

```
touch Movies.json
```

- Edit that file add the following **Json** code into it.

```json
{
    "Torrents": [
        {
            "Name": "NAMEOFTORRENT",
            ".Torrent": "DOWNLOADFORTORRENT",
            "MagnetUrl": "MagnetURL",
            "Icon": "https://github.com/HttpAnimation/RepoNEW/blob/main/REPLACEME?raw=true",
            "SeedersAtTheTime": "9",
            "Publisher": "WHOMADETHETORRENTFILE",
            "HasStreamURL": false,
            "StreamURL": "IFYOUCANSTREAMTHESHOWTHENADDTHEURLHERE",
            "Source": "WHERETHETORRENTCAMEOVER"
        }
    ]
}
```

- **Name** | The name of the torrent this can not be left blank.

```json
"Name": "Example Torrent",
```

- **.Torrent** | The .torrent download you can leave the filed blank for no download

```json
".Torrent": "https://example.com/torrents/torrentname.torrent",
```

```json
".Torrent": "",
```

- **MagnetUrl** | The magnet URL of the torrent this can be left blank for nothing

```json
"MagnetUrl": "magnet:?xt=urn:btih:EXAMPLEMAGNETURL",
```

```json
"MagnetUrl": "",
```

- **Icon** | This is the icon of the torrent. *Note: Photos don't work yet but they will be added in a future it's best just to add so when the update comes out you won't need to update your repo.*

```json
"Icon": "https://example.com/images/photo.png",
```

- **SeedersAtTheTime** | The amount of seeders at the time of making the torrent this does not have to be a number if the publisher has seeder eg. [Archive](https://archive.org)

```json
"SeedersAtTheTime": "69",
```

- **Publisher** | This is who made the torrent most of time you can use who made the real torrent from the info panel or the website you got it from but you can also your repos name.

```json
"Publisher": "Reposs",
```

- **HasStreamURL** | This is a ture or false statement false meaning the torrent is most likly not a video true meaning you can watch the video online this is usally linked up with a [Youtube](https://youtube.com) or [Archive](https://archive.org) video.

- **StreamURL** | This goes along with the **HasStreamURL** and is the URL to watch the video if you have **HasStreamURL** to false then you can just leave this blank if you have to true tho you will need to enter the URL to watch the thing.

```json
"StreamURL": "https://example.com/videos/video.mp4",
```

- **Source** | This is where the torrent came from this usally a [Archive](https://archive.org) URL.

```json
"Source": "https://gitlab.com/HttpAnimations/reposs"
```

## Special repos
Some repos need some more info.

## Oculus Quest

1) **TestedOn**
This should be placed after **Publisher**
```json
"TestedOn": "Q1 - Q2 - Q3 - QPro",
```

# Keyboard shortcuts
**command/ctrl + shift + p** : Dev tools

# More indev

## How the version number is displayed
To get the version number we use a **js** script to read a file called **package.json** witch has the **"version"** value using that value we send the data to things that need it. Here is a expmale on how it used on the index page.

```js
        // Function to fetch and display the version number from package.json
        function fetchVersion() {
            fetch('package.json')
            .then(response => response.json())
            .then(data => {
                const versionDiv = document.getElementById('version');
                versionDiv.innerHTML = 'Version: ' + data.version; // Assuming there's a "version" key in your package.json
            })
            .catch(error => console.error('Error loading the version:', error));
        }
```


# Credits

[fontawesome](https://fontawesome.com/)

# Distribution Guidelines for Reposs

If you wish to distribute Reposs, it's important to adhere to the official packaging guidelines. **Repackaging of any files, including but not limited to `.zip (Darwin), .rpm (Linux)`, `.deb (Linux)`, `.zip (Linux)` or `.exe (NT)` formats, is strictly prohibited**. You must utilize the packages provided by Reposs. If you need to package Reposs in a format not supported by the existing system, you are required to create a new project and compile the source code directly from the official repository, available here: [Reposs Official Repository](https://gitlab.com/HttpAnimations/reposs/-/tree/Repos?ref_type=heads).

For individual users who do not intend to distribute Reposs, you are permitted to compile from the main package. However, under no circumstances may you publish or redistribute the compiled files. This ensures compliance with Reposs distribution policies and respects the integrity of the software.