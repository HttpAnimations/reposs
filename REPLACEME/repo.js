// Assuming settings.js is included in your HTML as <script src="settings.js"></script>

function loadRepos() {
    window.electronAPI.readJsonFile('REPLACEME/Repo.json').then(data => {
        const repos = JSON.parse(data).urls;
        const list = document.getElementById('repoList');
        list.innerHTML = '';
        repos.forEach((url, index) => {
            const li = document.createElement('li');
            li.textContent = url;
            const removeBtn = document.createElement('button');
            removeBtn.textContent = 'Remove';
            removeBtn.onclick = () => removeRepo(index);
            li.appendChild(removeBtn);
            list.appendChild(li);
        });
    });
}

function addRepo() {
    const url = document.getElementById('newRepoUrl').value;
    if (url) {
        window.electronAPI.readJsonFile('REPLACEME/Repo.json').then(data => {
            const json = JSON.parse(data);
            json.urls.push(url);
            window.electronAPI.saveJsonFile('REPLACEME/Repo.json', JSON.stringify(json, null, 2)).then(() => {
                loadRepos();
                alert('Repo added successfully!');
            }).catch(err => {
                alert('Failed to add repo: ' + err);
            });
        });
    }
}

function removeRepo(index) {
    window.electronAPI.readJsonFile('REPLACEME/Repo.json').then(data => {
        const json = JSON.parse(data);
        json.urls.splice(index, 1);
        window.electronAPI.saveJsonFile('REPLACEME/Repo.json', JSON.stringify(json, null, 2)).then(() => {
            loadRepos();
            alert('Repo removed successfully!');
        }).catch(err => {
            alert('Failed to remove repo: ' + err);
        });
    });
}

document.addEventListener('DOMContentLoaded', loadRepos);
