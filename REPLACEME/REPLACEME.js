async function fetchJSON(url) {
    try {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    } catch (error) {
        console.error('Failed to fetch JSON from ' + url, error);
        return null; // Return null in case of failure to handle it gracefully
    }
}

async function loadTorrents() {
    const repoInfo = await fetchJSON('Repo.json'); // Adjust this URL if necessary
    if (!repoInfo || !repoInfo.urls || repoInfo.urls.length === 0) {
        console.error('Invalid repository info');
        document.getElementById('contentArea').innerHTML = '<p>Error fetching repository information. Check console for details.</p>';
        return;
    }

    repoInfo.urls.forEach(async (repoUrl) => {
        const torrentsData = await fetchJSON(repoUrl); // Fetching data from each URL provided in MRepo.json
        if (!torrentsData || !torrentsData.Torrents) {
            console.error('Invalid torrents data for URL:', repoUrl);
            return;
        }

        const contentArea = document.getElementById('contentArea');
        torrentsData.Torrents.forEach(torrent => {
            const torrentDiv = document.createElement('div');
            torrentDiv.className = 'torrent';
            torrentDiv.innerHTML = `
            <h4>${torrent.Name}</h4>
            <img src="${torrent.Icon}" class="torrent-icon">
            <p>Publisher: ${torrent.Publisher}</p>
            <div class="source-section">
                <p>Source: <a href="${torrent.Source}" target="_blank">Open Page</a></p>
            </div>
            ${torrent.HasStreamURL ? `<p>Stream: <a href="${torrent.StreamURL}" target="_blank">Watch Now</a></p>` : ''}
        `;
            contentArea.appendChild(torrentDiv);
        });
    });
}

loadTorrents();
