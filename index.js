const { app, BrowserWindow, globalShortcut, ipcMain } = require('electron');
const path = require('path');
const fs = require('fs');

function createWindow() {
  const mainWindow = new BrowserWindow({
    width: 1700,
    height: 900,
    webPreferences: {
      nodeIntegration: false,
      contextIsolation: true,
      preload: path.join(__dirname, 'preload.js')
    }
  });

  // Maximize the window
  mainWindow.maximize();

  mainWindow.loadFile('index.html');

  // Register a global shortcut that opens the DevTools
  globalShortcut.register('CommandOrControl+Shift+P', () => {
    mainWindow.webContents.openDevTools();
  });
}

app.whenReady().then(() => {
  createWindow();

  app.on('will-quit', () => {
    // Unregister all shortcuts when the application is about to quit
    globalShortcut.unregisterAll();
  });
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

ipcMain.handle('read-file', async (event, filePath) => {
  try {
    const content = fs.readFileSync(path.join(app.getAppPath(), filePath), 'utf8');
    return content;
  } catch (error) {
    console.error('Failed to read file:', error);
    throw error;
  }
});

ipcMain.handle('save-file', async (event, filePath, content) => {
  try {
    fs.writeFileSync(path.join(app.getAppPath(), filePath), content, 'utf8');
  } catch (error) {
    console.error('Failed to save file:', error);
    throw error;
  }
});
