document.addEventListener('DOMContentLoaded', function() {
    fetch('package.json')
    .then(response => response.json())
    .then(data => {
        const version = data.version;
        const contentElement = document.getElementById('content');
        contentElement.innerHTML = contentElement.innerHTML.replace('VERSIONNUMBER', version);
    })
    .catch(error => console.error('Error loading version:', error));
});