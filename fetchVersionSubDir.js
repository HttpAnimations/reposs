// Function to fetch and display the version number from package.json
function fetchVersion() {
    fetch('../package.json')
        .then(response => response.json())
        .then(data => {
            const versionDiv = document.getElementById('version');
            versionDiv.innerHTML = 'Version: ' + data.version; // Assuming there's a "version" key in your package.json
        })
        .catch(error => console.error('Error loading the version:', error));
}