const path = require('path');
const { FusesPlugin } = require('@electron-forge/plugin-fuses');
const { FuseV1Options, FuseVersion } = require('@electron/fuses');

module.exports = {
  packagerConfig: {
    // Asar is NEVER allowed to be used as it will break the repo manager beacuse it does not allow the abilty to edit files
    asar: false,
    icon: path.resolve(__dirname, 'images/icon.png') // Adjust the path to your icon file
  },
  makers: [
    {
      name: '@electron-forge/maker-squirrel',
      config: {
        iconUrl: path.resolve(__dirname, 'images/icon.ico'), // Windows icon
      },
    },
    {
      name: '@electron-forge/maker-zip',
      platforms: ['linux', 'darwin'],
      config: {
        icon: path.resolve(__dirname, 'images/icon.png') // macOS and Linux icon
      },
    },
    {
      name: '@electron-forge/maker-deb',
      config: {
        icon: path.resolve(__dirname, 'images/icon.png'), // Linux icon
        "compression": "gzip"
      },
    },
    {
      name: '@electron-forge/maker-rpm',
      config: {
        icon: path.resolve(__dirname, 'images/icon.png'), // Linux icon
      },
    }
  ],
  plugins: [
    new FusesPlugin({
      version: FuseVersion.V1,
      [FuseV1Options.RunAsNode]: false,
      [FuseV1Options.EnableCookieEncryption]: true,
      [FuseV1Options.EnableNodeOptionsEnvironmentVariable]: false,
      [FuseV1Options.EnableNodeCliInspectArguments]: false,
      [FuseV1Options.EnableEmbeddedAsarIntegrityValidation]: true,
      [FuseV1Options.OnlyLoadAppFromAsar]: false,
    }),
  ],
};
