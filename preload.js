const { contextBridge, ipcRenderer } = require('electron');

contextBridge.exposeInMainWorld('electronAPI', {
    readJsonFile: (filePath) => ipcRenderer.invoke('read-file', filePath),
    saveJsonFile: (filePath, content) => ipcRenderer.invoke('save-file', filePath, content)
});



document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('a[href]').forEach(link => {
        const url = link.href;
        if (url.startsWith('http://') || url.startsWith('https://')) {
            link.addEventListener('click', (e) => {
                e.preventDefault();
                shell.openExternal(url);
            });
        }
    });
});
